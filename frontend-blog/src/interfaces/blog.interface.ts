export interface filesPublications {
    fileBase64: string,
    name: string,
    type: string,
}

export interface formCreatePublication {
    title: string,
    description: string,
    files: filesPublications[]
}
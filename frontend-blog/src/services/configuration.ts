import axios from "axios";

export const instanceAxios = axios.create({
    baseURL: process.env.NEXT_PUBLIC_URL_BACKEND,
    timeout: 1000,
    headers: {'X-Custom-Header': 'foobar'}
});

instanceAxios.interceptors.request.use(function (config) {
    // Do something before request is sent
    config.headers.Authorization = `Bearer ${localStorage.getItem('custom-auth-token')}`;
    return config;
}, function (error) {
// Do something with request error
    return Promise.reject(error);
});


// instanceAxios.interceptors.response.use((response) => response, (error) => {
    
// });

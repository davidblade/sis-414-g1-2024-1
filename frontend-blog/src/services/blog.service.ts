import { formCreatePublication } from "@/interfaces/blog.interface";
import { instanceAxios } from "./configuration";
interface Login {
    username: string,
    password: string
}
export const loginAuth = async ({ username, password }: Login) => {
    const login = await instanceAxios.post('/auth/login', { username, password }).then((response) => {
        console.log(response)
        return response.data
    }).catch((error: any) => {
        alert(error.response.data.result)
    })
    return login
}

export const createPublication = async (form: formCreatePublication) => {
    const publication = await instanceAxios.post('/publication/create-blog', form).then((response) => {
        console.log(response.data)
        return response.data
    }).catch((error: any) => {
        alert(error.response.data.result)
    })
    return publication
}
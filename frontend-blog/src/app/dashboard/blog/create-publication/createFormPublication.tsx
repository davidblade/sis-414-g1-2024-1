'use client';

import * as React from 'react';
import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardHeader from '@mui/material/CardHeader';
import Divider from '@mui/material/Divider';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import OutlinedInput from '@mui/material/OutlinedInput';
import Select from '@mui/material/Select';
import Grid from '@mui/material/Unstable_Grid2';
import { styled } from '@mui/material/styles';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';
import { formCreatePublication } from '@/interfaces/blog.interface';
import { createPublication } from '@/services/blog.service';

const VisuallyHiddenInput = styled('input')({
  clip: 'rect(0 0 0 0)',
  clipPath: 'inset(50%)',
  height: 1,
  overflow: 'hidden',
  position: 'absolute',
  bottom: 0,
  left: 0,
  whiteSpace: 'nowrap',
  width: 1,
});


export function CreateFormPublication() {
  const [form, setForm] = React.useState<formCreatePublication>({
    title: "",
    description: "",
    files: []
  })

  //en event esta llegando todo el INPUT (select, input, textarea, etc)
  const handleChange = (event: any) => {
    setForm({
      ...form,//title:"",description:"",files:[]
      [event.target.name]: event.target.value//el valor del input, modificando el campo que tiene en form
      //[title, description]
    });
  }
  const handleChangeFile = (event: any) => {
    Array.from(event.target.files).map((file: any) => {
      const reader = new FileReader()
      reader.readAsDataURL(file)
      reader.onload = () => {
        setForm({
          ...form,//<-
          files: [
            ...form.files, //{},{},{},{}
            {
              fileBase64: reader.result as string,
              name: file.name,
              type: file.type
            }//,{}
          ]
        })
      }
    })
  }

  const submitForm = async (event: any) => {
    event.preventDefault();
    console.log("ENVIANDO FORMULARIO")
    console.log(form)
    await createPublication(form)
  }

  return (
    <form
      onSubmit={submitForm}
    >
      <Card>
        <CardHeader title="Publicaciones" subheader="Crear publicaciones en mi Blog" />
        <Divider />
        <CardContent>
          <Grid container spacing={3}>
            <Grid md={6} xs={12}>
              <FormControl fullWidth required>
                <InputLabel>Titulo</InputLabel>
                <OutlinedInput
                  label="Titulo"
                  name="title"
                  value={form?.title}
                  onChange={(e) => handleChange(e)}
                />
              </FormControl>
            </Grid>
            <Grid md={6} xs={12}>
              <FormControl fullWidth required>
                <InputLabel>Descripcion</InputLabel>
                <OutlinedInput
                  label="Descripcion"
                  name="description"
                  value={form?.description}
                  onChange={(e) => handleChange(e)}
                />
              </FormControl>
            </Grid>
            <Grid md={6} xs={12}>
              <Button
                component="label"
                role={undefined}
                variant="contained"
                tabIndex={-1}
                startIcon={<CloudUploadIcon />}
              >
                Upload file
                <VisuallyHiddenInput type="file" name='files' onChange={(e) => handleChangeFile(e)} />
              </Button>
            </Grid>
            {/* <Grid md={6} xs={12}>
              <FormControl fullWidth required>
                <InputLabel>Email address</InputLabel>
                <OutlinedInput defaultValue="sofia@devias.io" label="Email address" name="email" />
              </FormControl>
            </Grid>
            <Grid md={6} xs={12}>
              <FormControl fullWidth>
                <InputLabel>Phone number</InputLabel>
                <OutlinedInput label="Phone number" name="phone" type="tel" />
              </FormControl>
            </Grid>
            <Grid md={6} xs={12}>
              <FormControl fullWidth>
                <InputLabel>State</InputLabel>
                <Select defaultValue="New York" label="State" name="state" variant="outlined">
                  {states.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>
            <Grid md={6} xs={12}>
              <FormControl fullWidth>
                <InputLabel>City</InputLabel>
                <OutlinedInput label="City" />
              </FormControl>
            </Grid> */}
          </Grid>
        </CardContent>
        <Divider />
        <CardActions sx={{ justifyContent: 'flex-end' }}>
          <Button type="submit" variant="contained">Save details</Button>
        </CardActions>
      </Card>
    </form>
  );
}

import { Typography } from "@mui/material"
import { Stack } from "@mui/system"
import Grid from '@mui/material/Grid';
import { CreateFormPublication } from "./createFormPublication";

const CreatePublication = () => {
    return (
        <Stack spacing={3}>
            <div>
                <Typography variant="h4">Account</Typography>
            </div>
            <Grid container spacing={3}>
                <Grid item lg={12} md={12} xs={12}>
                    <CreateFormPublication/>
                </Grid>
            </Grid>
        </Stack>
    )
}

export default CreatePublication
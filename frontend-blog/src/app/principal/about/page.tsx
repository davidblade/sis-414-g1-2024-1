import GroupInput from "../components/GroupInput";
import Image from "next/image";

export default function About(){
    return (
        <section className="banner">
                <div>
                    <h1>About <span>Us</span></h1>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque ab itaque nam, quasi accusamus id velit magni suscipit, consectetur a vel. Quibusdam quaerat non, quas id molestias aspernatur veniam atque.</p>
                    {/* <GroupInput placeholder="Search for a location..." textbtn="Continue"></GroupInput> */}
                    <GroupInput icon={false} placeholder="Email Address" textbtn="Send" buttonSecondary></GroupInput>
                </div>
                <div>
                    <Image src="/images/bg-hero-2.svg" alt="" width={100} height={100} />
                </div>
        </section>
    )
}
import Service from "../components/Service";
import { Tracks } from "../components/Tracks";

export default function Courses(){
    return (
        <div>
            <section id="tracks">
                <Tracks subtitle="UI/UX Design"title="UI/UX Design for Beginners" price={98} stars={3}></Tracks>
                <Tracks subtitle="UI/UX Design"title="UI/UX Design for Beginners" price={198} stars={2}></Tracks>
                <Tracks subtitle="UI/UX Design"title="UI/UX Design for Beginners" price={928} stars={4.2}></Tracks>
            </section>
            <section id="tracks">
                <Tracks subtitle="UI/UX Design"title="UI/UX Design for Beginners" price={98} stars={3}></Tracks>
                <Tracks subtitle="UI/UX Design"title="UI/UX Design for Beginners" price={198} stars={2}></Tracks>
                <Tracks subtitle="UI/UX Design"title="UI/UX Design for Beginners" price={928} stars={4.2}></Tracks>
            </section>
            <section id="tracks">
                <Tracks subtitle="UI/UX Design"title="UI/UX Design for Beginners" price={98} stars={3}></Tracks>
                <Tracks subtitle="UI/UX Design"title="UI/UX Design for Beginners" price={198} stars={2}></Tracks>
                <Tracks subtitle="UI/UX Design"title="UI/UX Design for Beginners" price={928} stars={4.2}></Tracks>
            </section>
        </div>
    )
}
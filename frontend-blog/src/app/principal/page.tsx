import styles from "./page.module.css";
import Service from "./components/Service";
import Comments from "./components/Comment";
import { Tracks } from "./components/Tracks";
import GroupInput from "./components/GroupInput";
import Title from "./components/Title";
import Image from "next/image";
// function About(){
//   return <h1>Hola mundo desde  nose cauq</h1>
// }

export default function Home() {
  return (
    <main>
        <div className="bg-banner"></div>
        <section className="banner">
                <div>
                    <h1>The <span>Smart</span></h1>
                    <h1>Choice For <span>Future</span></h1>
                    <p>Elearn is a global training provider based across the UK that specialises in accredited and bespoke training courses. We crush the...</p>
                    <GroupInput placeholder="Search for a location..." textbtn="Continue"></GroupInput>
                    {/* <GroupInput icon={false} placeholder="Email Address" textbtn="Send" buttonSecondary></GroupInput> */}
                </div>
                <div>
                    <Image src="/images/bg-hero.svg" alt="" width={100} height={100} />
                </div>
        </section>
        
        <section id="services">
            <Service img="/images/service-1.svg" title="Learn The Latest Skills" paragraph="Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a BC, making it over 2000 years old."></Service>
            <Service img="/images/service-1.svg" title="Get Ready For a Career" paragraph="Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a BC, making it over 2000 years old."/>
            <Service img="/images/service-1.svg" title="Earn a Certificate" paragraph="Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a BC, making it over 2000 years old."/>
        </section>

        <Title title="Our Tracks" description="Lorem Ipsum is simply dummy text of the printing."></Title>

        <section id="tracks">
            <Tracks subtitle="UI/UX Design"title="UI/UX Design for Beginners" price={98} stars={3}></Tracks>
            <Tracks subtitle="UI/UX Design"title="UI/UX Design for Beginners" price={198} stars={2}></Tracks>
            <Tracks subtitle="UI/UX Design"title="UI/UX Design for Beginners" price={928} stars={4.2}></Tracks>
        </section>

        <Title title="What student’s say" description="Lorem Ipsum is simply dummy text of the printing."></Title>

        <section id="commints">
          <Comments img="/images/people0.png" namePeople="Finlay Kirk" description="Web Developper" comment="Teachings of the great explore of truth, the master-builder of human happiness. no one rejects,dislikes, or avoids pleasure itself, pleasure itself" />
          <Comments img="/images/people1.png" namePeople="Dannette P. Cervantes" description="Web Design" comment="Complete account of the system and expound the actual Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots"/>
          <Comments img="/images/people2.png" namePeople="Clara R. Altman" description="UI&UX Design" comment="There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form,by injected humour"/>
        </section>
      {/* <About></About> */}
    </main>

    // <main className={styles.main}>
    //   <div className={styles.description}>
    //     <p>
    //       Get started by editing&nbsp;
    //       <code className={styles.code}>src/app/page.tsx</code>
    //     </p>
    //     <div>
    //       <a
    //         href="https://vercel.com?utm_source=create-next-app&utm_medium=appdir-template&utm_campaign=create-next-app"
    //         target="_blank"
    //         rel="noopener noreferrer"
    //       >
    //         By{" "}
    //         <Image
    //           src="/vercel.svg"
    //           alt="Vercel Logo"
    //           className={styles.vercelLogo}
    //           width={100}
    //           height={24}
    //           priority
    //         />
    //       </a>
    //     </div>
    //   </div>

    //   <div className={styles.center}>
    //     <Image
    //       className={styles.logo}
    //       src="/next.svg"
    //       alt="Next.js Logo"
    //       width={180}
    //       height={37}
    //       priority
    //     />
    //   </div>

    //   <div className={styles.grid}>
    //     <a
    //       href="https://nextjs.org/docs?utm_source=create-next-app&utm_medium=appdir-template&utm_campaign=create-next-app"
    //       className={styles.card}
    //       target="_blank"
    //       rel="noopener noreferrer"
    //     >
    //       <h2>
    //         Docs <span>-&gt;</span>
    //       </h2>
    //       <p>Find in-depth information about Next.js features and API.</p>
    //     </a>

    //     <a
    //       href="https://nextjs.org/learn?utm_source=create-next-app&utm_medium=appdir-template&utm_campaign=create-next-app"
    //       className={styles.card}
    //       target="_blank"
    //       rel="noopener noreferrer"
    //     >
    //       <h2>
    //         Learn <span>-&gt;</span>
    //       </h2>
    //       <p>Learn about Next.js in an interactive course with&nbsp;quizzes!</p>
    //     </a>

    //     <a
    //       href="https://vercel.com/templates?framework=next.js&utm_source=create-next-app&utm_medium=appdir-template&utm_campaign=create-next-app"
    //       className={styles.card}
    //       target="_blank"
    //       rel="noopener noreferrer"
    //     >
    //       <h2>
    //         Templates <span>-&gt;</span>
    //       </h2>
    //       <p>Explore starter templates for Next.js.</p>
    //     </a>

    //     <a
    //       href="https://vercel.com/new?utm_source=create-next-app&utm_medium=appdir-template&utm_campaign=create-next-app"
    //       className={styles.card}
    //       target="_blank"
    //       rel="noopener noreferrer"
    //     >
    //       <h2>
    //         Deploy <span>-&gt;</span>
    //       </h2>
    //       <p>
    //         Instantly deploy your Next.js site to a shareable URL with Vercel.
    //       </p>
    //     </a>
    //   </div>
    // </main>
  );
}

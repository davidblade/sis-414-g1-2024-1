'use client'

import { useRouter } from "next/navigation";
import Button from "./Button";
import GroupInput from "./GroupInput";
import { Logo } from "./Logo";
import Link from 'next/link'

export function Header(){
const router = useRouter()
const handleClickLogin = () => {
    router.push('/auth/sign-in')
}
    return (
        <header>
            <nav className="header-nav">
                <Logo />
                <div>
                    <div>
                        <Link href="/">Home</Link>
                        <Link href="/about/">About us</Link>
                        <Link href="/courses/">Courses</Link>
                        <a href="">Our Service</a>
                        <a href="">Contact us</a>
                    </div>
                    <Button name="Sign In" onClick={handleClickLogin}></Button>
                </div>
            </nav>
        {/* <Button name="Join curse" secondary/> */}
        
        </header>
    )
}


{/* <div className="banner">
                <div>
                    <h1>The <span>Smart</span></h1>
                    <h1>Choice For <span>Future</span></h1>
                    <p>Elearn is a global training provider based across the UK that specialises in accredited and bespoke training courses. We crush the...</p>
                    <GroupInput placeholder="Search for a location..." textbtn="Continue"></GroupInput>
                    {/* <GroupInput icon={false} placeholder="Email Address" textbtn="Send" buttonSecondary></GroupInput> */}
                {/*</div>
                <div>
                    <Image src="/images/bg-hero.svg" alt="" />
                </div>
        </div> */}
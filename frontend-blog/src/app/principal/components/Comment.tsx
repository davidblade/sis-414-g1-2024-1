import Image from "next/image";

export default function Comments(props:{img:string,comment:string,namePeople:string,description:string}){
    return (
        <div className="item-comment">
             <p>{props.comment}</p>
             <div>
                <Image src={props.img} alt="image" width={100} height={100}/>
                <div>
                    <h4>{props.namePeople}</h4>
                    <p>{props.description}</p>
                </div>
             </div>
        </div>
    )
}
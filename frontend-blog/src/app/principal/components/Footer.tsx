import { Logo } from "./Logo";

export default function Footer(){
    return (
        <footer>
            <div>
                <div>
                    <Logo />
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy a type specimen book.</p>
                </div>
                <div>
                    <h4>Company</h4>
                    <ul>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">How to work?</a></li>
                        <li><a href="#">Populer Course</a></li>
                        <li><a href="#">Service</a></li>
                    </ul>
                </div>
                <div>
                <h4>Company</h4>
                    <ul>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">How to work?</a></li>
                        <li><a href="#">Populer Course</a></li>
                        <li><a href="#">Service</a></li>
                    </ul>
                </div>
                <div>
                <h4>Company</h4>
                    <ul>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">How to work?</a></li>
                        <li><a href="#">Populer Course</a></li>
                        <li><a href="#">Service</a></li>
                    </ul>
                </div>
                <div>
                <h4>Company</h4>
                    <ul>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">How to work?</a></li>
                        <li><a href="#">Populer Course</a></li>
                        <li><a href="#">Service</a></li>
                    </ul>
                </div>
            </div>
            <div>
                <p>BookStore All Right Reserved, 2024</p>
            </div>
        </footer>
    )
}
import Image from "next/image";

export default function Service(props:{img:string,title:string,paragraph:string}){
    return (
        <div className="item-service">
            <div>
                <Image src={props.img} alt="" width={100} height={100}/>
            </div>
            <div>
                <h3>{props.title}</h3>
                <p>{props.paragraph}</p>
            </div>
        </div>
    )
}
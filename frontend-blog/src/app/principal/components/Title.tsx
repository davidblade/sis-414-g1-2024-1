const Title = ({title,description}:{title:string,description:string})=>{
    return (
        <div className="title">
            <h2>{title}</h2>
            <p>{description}</p>
        </div>
    )
}

export default Title
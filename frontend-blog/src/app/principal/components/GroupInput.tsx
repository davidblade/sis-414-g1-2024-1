import Button from "./Button";
import { Search } from 'react-feather';

export default function GroupInput(props:{placeholder:string,textbtn:string,icon?:boolean,buttonPrimary?:boolean,buttonSecondary?:boolean}){
    const defaultValues = {icon:true,placeholder:"",buttonPrimary:props.buttonSecondary?false:true,buttonSecondary:props.buttonPrimary?false:true}
    const setprops = {...defaultValues,...props}
    return (
        <div className="group-input">
            {setprops.icon?<Search size={18}/>:''}
            <input type="text" placeholder={props.placeholder} />
            <Button name={props.textbtn} primary={setprops.buttonPrimary} secondary={setprops.buttonSecondary}></Button>
        </div>
    )
}
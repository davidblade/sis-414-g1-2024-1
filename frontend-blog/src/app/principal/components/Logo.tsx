import Image from "next/image";

export function Logo(){
    return (
        <div className="logo-page">
            <Image src="/images/logo.svg" alt="logo" width={100} height={100} />
            <h3>Book Store</h3>
        </div>
    )
}
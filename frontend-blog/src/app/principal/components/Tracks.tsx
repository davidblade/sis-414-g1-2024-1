import Image from "next/image";
import Button from "./Button";

export function Tracks(props:{subtitle:string,title:string,price:number,stars:number}){
    const prepareStars = [0,0,0,0,0].map((e,i)=>{
        return (i+1)<=Math.round(props.stars)?1:0
    })
    const renderStars = prepareStars.map(e=>{
        return e?<i className="icon-star-1"></i>:<i className="icon-star-0"></i>
    })
    return (
        <div className="item-track">
            <Image src="/images/track-1.png" alt="" width={100} height={100}/>
            <div className="detail-track">
                <div>
                    <p>
                        <span>{props.subtitle}</span>
                        <span>
                            {renderStars}
                        </span>
                    </p>
                    <h3>{props.title}</h3>
                    <h3>${props.price}</h3>
                </div>
                <div>
                    horas, minutos
                </div>
            </div>
            <div>
                <Button name="Join Course" secondary></Button>
            </div>
        </div>
    )
}
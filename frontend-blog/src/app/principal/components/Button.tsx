export default function Button(props:{name:string,primary?:boolean,secondary?:boolean,onClick?:()=>void}){
    const defaultsValue  = {primary:props.secondary?false:true,secondary:props.primary?false:true}
    const setprops = {...defaultsValue,...props}
    return (
        setprops.primary?
        <button className="btn btn-primary" onClick={props.onClick}>{setprops.name}</button>
        :setprops.secondary?
        <button className="btn btn-secondary" onClick={props.onClick}>{setprops.name}</button>
        :
        <button className="btn" onClick={props.onClick}>{setprops.name}</button>
    )
}
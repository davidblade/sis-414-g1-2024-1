import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { HydratedDocument } from 'mongoose';

export type RolesAssignmentUserDocument = HydratedDocument<RolesAssignmentUser>;

@Schema({ timestamps: true})
export class RolesAssignmentUser {
  @Prop()
  roleID: string;

  @Prop()
  userID: string;
  
  @Prop({default:true})
  active:boolean;
}

export const RolesAssignmentUserSchema = SchemaFactory.createForClass(RolesAssignmentUser);

export type PermissionsDocument = HydratedDocument<Permissions>;

@Schema({ timestamps: true })
export class Permissions {
  @Prop()
  name: string;

  @Prop({default:true})
  active:boolean;
}

export const PermissionsSchema = SchemaFactory.createForClass(Permissions);

export type RolesDocument = HydratedDocument<Roles>;
@Schema({ timestamps: true })
export class Roles {
  @Prop()
  name: string;

  @Prop()
  description: string;

  @Prop({ type: [mongoose.Schema.Types.ObjectId], ref: 'Permissions'})
  permissions: mongoose.Schema.Types.ObjectId[];

  @Prop({default:true})
  active:boolean;
}

export const RolesSchema = SchemaFactory.createForClass(Roles);
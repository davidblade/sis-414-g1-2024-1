import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type userDocument = HydratedDocument<User>;

export class ProfileUser
{
  firsName: string;
  lastName: string;
  phone:string
}

@Schema({ timestamps: true, collection: 'user' })
export class User {
  @Prop()
  username: string;

  @Prop()
  password: string;

  @Prop()
  profile: ProfileUser;

  @Prop({default:true})
  active:boolean;
}

export const UserSchema = SchemaFactory.createForClass(User);
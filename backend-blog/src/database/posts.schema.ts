import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type PostDocument = HydratedDocument<Post>;

export class files{
  _id:string
  nameOriginal:string
  name:string
  type:string
}

@Schema({timestamps:true})
export class Post {
  @Prop()
  title: string;

  @Prop()
  description: string;

  @Prop()
  user: string;

  @Prop({type:[files],default:[]})
  files:files[];

  @Prop({default:true})
  active:boolean;

  @Prop()
  createdAt:Date
}

export const PostSchema = SchemaFactory.createForClass(Post);
import { SetMetadata } from '@nestjs/common';
export const KEY_METADATA_PERMISSIONS="permissions"
export const Permissions = (...args: string[]) => SetMetadata(KEY_METADATA_PERMISSIONS, args);
import { createParamDecorator, ExecutionContext } from '@nestjs/common';
export interface Authenticate {
    username:string,
    userId:string,
    token:string
}
export const UserAuthenticate = createParamDecorator(
  (data: unknown, ctx: ExecutionContext):Authenticate => {
    const request = ctx.switchToHttp().getRequest();
    return {
        username:request.decodedData.username,
        userId:request.decodedData.userId,
        token:request.token
    }
  },
);
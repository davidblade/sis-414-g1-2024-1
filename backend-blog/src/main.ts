import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { bold } from 'chalk';
import { ValidationPipe } from '@nestjs/common';
import { ResponseInterceptor } from './interceptor/response.interceptor';
import { json, urlencoded } from 'express';
async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  //LIMITE DE TRANSFERENCIA DE DATOS
  app.use(json({ limit: '50mb' }));
  app.use(urlencoded({ extended: true, limit: '50mb' }));
  const config = new DocumentBuilder()
    .setTitle('Mis servicios de mi Blog')
    .setDescription('Estos servicios son los que tengo en mi blog')
    .setVersion('1.0')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);
  app.useGlobalPipes(new ValidationPipe());
  app.useGlobalInterceptors(new ResponseInterceptor());
  console.log(process.env.ORIGIN)
  app.enableCors(); 
  // app.enableCors({
  //   origin: process.env.ORIGIN || '*',
  //   methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
  //   allowedHeaders: 'Content-Type, Accept',
  // });
  await app.listen(process.env.PORT, '0.0.0.0').then(async () => {
    console.log(bold.blue('Server Running in', (await app.getUrl()) + '/api'));
  });
}
bootstrap();

import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { UserSchema,User} from 'src/database/users.schema';
import { Permissions,PermissionsSchema, Roles, RolesAssignmentUser, RolesAssignmentUserSchema, RolesSchema } from 'src/database/roles.schema';

@Module({
  imports:[
    MongooseModule.forFeature([
      { name: User.name, schema: UserSchema },
      { name: Roles.name, schema: RolesSchema },
      { name: Permissions.name, schema: PermissionsSchema },
      { name: RolesAssignmentUser.name, schema: RolesAssignmentUserSchema }
    ])
  ],
  controllers: [UserController],
  providers: [UserService],
  exports: [UserService],
})
export class UserModule {}

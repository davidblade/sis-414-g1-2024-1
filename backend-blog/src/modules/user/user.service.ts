import { Injectable } from '@nestjs/common';
import { CreateUserDTO } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { InjectModel } from '@nestjs/mongoose';
import { User } from 'src/database/users.schema';
import { Model } from 'mongoose';
import * as bcrypt from 'bcrypt';
import { Permissions, Roles, RolesAssignmentUser } from 'src/database/roles.schema';
import { AssignmentRoleUserDTO, CreatePermissionsDTO, CreateRoleDTO } from './dto/role.dto';

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User.name) private userModel: Model<User>,
    @InjectModel(Roles.name) private rolesModel: Model<Roles>,
    @InjectModel(Permissions.name) private permissionsModel: Model<Permissions>,
    @InjectModel(RolesAssignmentUser.name) private rolesAssignmentUserModel: Model<RolesAssignmentUser>,
  ){}

  async createPermissions(createPermissions:CreatePermissionsDTO){
     const newPermission = new this.permissionsModel({
      name:createPermissions.name
     })
     return await newPermission.save()
  }

  async getPermissions(){
    const permissions = await this.permissionsModel.find({active:true})
    return permissions
  }

  async createRoles(createRoleDTO:CreateRoleDTO){
      const newRole = await this.rolesModel.create(createRoleDTO)
      return newRole
  }

  async getRoles(){
    const roles = await this.rolesModel.find({active:true})
    return roles
  }

  async asignUserRoles(assignmentRoleUserDTO:AssignmentRoleUserDTO){
    const newAssignmentRole = await this.rolesAssignmentUserModel.create(assignmentRoleUserDTO)
    return newAssignmentRole
  }

  async getPermissionsUser(userID:string){
    const asignUserRoles = await this.rolesAssignmentUserModel.find({userID})
    //asignUserRoles.map(e=>e.roleID) => [id,id,id]
    const getRolesUser = await this.rolesModel.find({_id:{$in:asignUserRoles.map(e=>e.roleID.toString())}})
    const getIdsPermissions = []
    for(const role of getRolesUser){
      for(const permission of role.permissions){
        getIdsPermissions.push(permission)
      }
    }
    const getPermissons = await this.permissionsModel.find({_id:{$in:getIdsPermissions}})
    return getPermissons.map(e=>e.name)
  }

  async createUser(createUserDTO: CreateUserDTO) {
    
    const hashPassword = await bcrypt.hash(createUserDTO.password, 10);
    //sin encriptar el password
    //const newUser = new this.userModel(createUserDTO)

    //encriptar el password metodo 1
    // const newUser = new this.userModel()
    // newUser.username = createUserDTO.username
    // newUser.password = hashPassword
    // newUser.profile = createUserDTO.profile

    //encriptar el password metodo 2
    const newUser = new this.userModel({...createUserDTO,password:hashPassword})

    return newUser.save();
  }

  findAll() {
    return `This action returns all user`;
  }

  async findOneUsername(username: string) {
    try {
      //const getUser = await this.userModel.findOne({username:username})
      const getUser = await this.userModel.findOne({username})
      if(!getUser)
        throw "Usuario no encontrado"
      return getUser 
    } catch (error) {
      throw error
    }
  }

  findOneId(id: string) {
    const getUser = this.userModel.findOne({_id:id})
    return getUser
  }

  update(id: string, updateUserDto: UpdateUserDto) {
    return `This action updates a #${id} user`;
  }

  remove(id: string) {
    return `This action removes a #${id} user`;
  }
}

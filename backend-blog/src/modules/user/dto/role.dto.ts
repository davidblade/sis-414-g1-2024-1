import { ApiProperty } from "@nestjs/swagger"
import { IsArray, IsString } from "class-validator"

export class CreatePermissionsDTO{
    @ApiProperty()
    @IsString()
    name:string
}

export class CreateRoleDTO{
    @ApiProperty()
    @IsString()
    name:string
    @ApiProperty()
    @IsString()
    description:string
    @ApiProperty({type:[String]})
    @IsArray()
    permissions:string[]
}

export class AssignmentRoleUserDTO{
    @ApiProperty()
    @IsString()
    roleID:string

    @ApiProperty()
    @IsString()
    userID:string
}
import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards } from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDTO } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/guards/jwt-auth.guard';
import {AssignmentRoleUserDTO, CreatePermissionsDTO,CreateRoleDTO} from './dto/role.dto'
import { Permissions } from 'src/decorators/permissions.decorator';
import { PermissionsGuard } from 'src/guards/permission.guard';

@ApiTags('Usuarios')
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @ApiBearerAuth()//para que aparesca el candadito de swagger y podamos colocar el token
  ///['permiso_1'] estos permisos estan en el metadata
  @Permissions('crear_permisos')//PERMISOS QUE DEBE TENER EL USUARIO PARA PODER CONSUMIR ESTE SERVICIO
  @UseGuards(JwtAuthGuard,PermissionsGuard)//GUARDIANES PARA VALIDAR JwtAuthGuard=TOKEN ES VALIDO; PermissionsGuard=LOS PERMISOS DEL USUARIO
  @Post('create-permissions')
  async createPermissions(@Body() createPermissions:CreatePermissionsDTO){
    try {
      return await this.userService.createPermissions(createPermissions)
    } catch (error) {
      throw error
    }
  }

  @ApiBearerAuth()//para que aparesca el candadito de swagger y podamos colocar el token
  ///['permiso_1'] estos permisos estan en el metadata
  @Permissions('ver_permisos')//PERMISOS QUE DEBE TENER EL USUARIO PARA PODER CONSUMIR ESTE SERVICIO
  @UseGuards(JwtAuthGuard,PermissionsGuard)//GUARDIANES PARA VALIDAR JwtAuthGuard=TOKEN ES VALIDO; PermissionsGuard=LOS PERMISOS DEL USUARIO
  @Get('all-permissions')
  async getAllPermissions(){
    try {
      return await this.userService.getPermissions()
    } catch (error) {
      throw error
    }
  }

  @ApiBearerAuth()//para que aparesca el candadito de swagger y podamos colocar el token
  ///['permiso_1'] estos permisos estan en el metadata
  @Permissions('crear_rol')//PERMISOS QUE DEBE TENER EL USUARIO PARA PODER CONSUMIR ESTE SERVICIO
  @UseGuards(JwtAuthGuard,PermissionsGuard)//GUARDIANES PARA VALIDAR JwtAuthGuard=TOKEN ES VALIDO; PermissionsGuard=LOS PERMISOS DEL USUARIO
  @Post('create-role')
  async createRoles(@Body() createRoleDTO:CreateRoleDTO){
    try {
      return await this.userService.createRoles(createRoleDTO)
    } catch (error) {
      throw error
    }
  }

  @ApiBearerAuth()//para que aparesca el candadito de swagger y podamos colocar el token
  ///['permiso_1'] estos permisos estan en el metadata
  @Permissions('ver_roles')//PERMISOS QUE DEBE TENER EL USUARIO PARA PODER CONSUMIR ESTE SERVICIO
  @UseGuards(JwtAuthGuard,PermissionsGuard)//GUARDIANES PARA VALIDAR JwtAuthGuard=TOKEN ES VALIDO; PermissionsGuard=LOS PERMISOS DEL USUARIO
  @Get('all-roles')
  async getAllRoles(){
    try {
      return await this.userService.getRoles()
    } catch (error) {
      throw error
    }
  }

  @ApiBearerAuth()//para que aparesca el candadito de swagger y podamos colocar el token
  ///['permiso_1'] estos permisos estan en el metadata
  @Permissions('asignar_rol')//PERMISOS QUE DEBE TENER EL USUARIO PARA PODER CONSUMIR ESTE SERVICIO
  @UseGuards(JwtAuthGuard,PermissionsGuard)//GUARDIANES PARA VALIDAR JwtAuthGuard=TOKEN ES VALIDO; PermissionsGuard=LOS PERMISOS DEL USUARIO
  @Post('assignment-role-user')
  async asignUserRoles(@Body() assignmentRoleUserDTO:AssignmentRoleUserDTO){
    try {
      return await this.userService.asignUserRoles(assignmentRoleUserDTO)
    } catch (error) {
      throw error
    }
  }

  @ApiBearerAuth()//para que aparesca el candadito de swagger y podamos colocar el token
  ///['permiso_1'] estos permisos estan en el metadata
  @Permissions('crear_usuario')//PERMISOS QUE DEBE TENER EL USUARIO PARA PODER CONSUMIR ESTE SERVICIO
  @UseGuards(JwtAuthGuard,PermissionsGuard)//GUARDIANES PARA VALIDAR JwtAuthGuard=TOKEN ES VALIDO; PermissionsGuard=LOS PERMISOS DEL USUARIO
  @Post('crear-usario')
  createUser(@Body() createUserDto: CreateUserDTO) {
    return this.userService.createUser(createUserDto);
  }

  @ApiBearerAuth()//para que aparesca el candadito de swagger y podamos colocar el token
  ///['permiso_1'] estos permisos estan en el metadata
  @Permissions('ver_usuario')//PERMISOS QUE DEBE TENER EL USUARIO PARA PODER CONSUMIR ESTE SERVICIO
  @UseGuards(JwtAuthGuard,PermissionsGuard)//GUARDIANES PARA VALIDAR JwtAuthGuard=TOKEN ES VALIDO; PermissionsGuard=LOS PERMISOS DEL USUARIO
  @Get()
  findAll() {
    return this.userService.findAll();
  }

  @ApiBearerAuth()//para que aparesca el candadito de swagger y podamos colocar el token
  ///['permiso_1'] estos permisos estan en el metadata
  @Permissions('ver_usuario')//PERMISOS QUE DEBE TENER EL USUARIO PARA PODER CONSUMIR ESTE SERVICIO
  @UseGuards(JwtAuthGuard,PermissionsGuard)//GUARDIANES PARA VALIDAR JwtAuthGuard=TOKEN ES VALIDO; PermissionsGuard=LOS PERMISOS DEL USUARIO
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.userService.findOneId(id);
  }

  @ApiBearerAuth()//para que aparesca el candadito de swagger y podamos colocar el token
  ///['permiso_1'] estos permisos estan en el metadata
  @Permissions('editar_usuario')//PERMISOS QUE DEBE TENER EL USUARIO PARA PODER CONSUMIR ESTE SERVICIO
  @UseGuards(JwtAuthGuard,PermissionsGuard)//GUARDIANES PARA VALIDAR JwtAuthGuard=TOKEN ES VALIDO; PermissionsGuard=LOS PERMISOS DEL USUARIO
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.userService.update(id, updateUserDto);
  }

  @ApiBearerAuth()//para que aparesca el candadito de swagger y podamos colocar el token
  ///['permiso_1'] estos permisos estan en el metadata
  @Permissions('eliminar_usuario')//PERMISOS QUE DEBE TENER EL USUARIO PARA PODER CONSUMIR ESTE SERVICIO
  @UseGuards(JwtAuthGuard,PermissionsGuard)//GUARDIANES PARA VALIDAR JwtAuthGuard=TOKEN ES VALIDO; PermissionsGuard=LOS PERMISOS DEL USUARIO
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.userService.remove(id);
  }
}

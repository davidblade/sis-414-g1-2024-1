import { Injectable } from '@nestjs/common';
import { CreateAuthDto, LoginDTO } from './dto/create-auth.dto';
import { UpdateAuthDto } from './dto/update-auth.dto';
import { UserService } from '../user/user.service';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {

  constructor(
    private userService: UserService,
    private jwtTokenService: JwtService
  ){}

  async loginWithCredentials(user: LoginDTO) {
    try {
      const getUser = await this.userService.findOneUsername(user.username)
      const isMatch = await bcrypt.compare(user.password, getUser.password);
      if (!isMatch) {
        throw "Contraseña incorrecta"
      }else{
        const getPermissions = await this.userService.getPermissionsUser(getUser._id.toString())
        const payload = { 
          username: getUser.username,
          userId: getUser._id,
          permissions:getPermissions
        };
        return {
          access_token: this.jwtTokenService.sign(payload),
        };  
      }
    } catch (error) {
      console.log(error)
      throw error
    }
  }

  async verifyToken(token:string) {
    const verifytoken = await this.jwtTokenService.verify(token, {
      secret : process.env.TOKENSECRET
    });
    return verifytoken
  } 
}

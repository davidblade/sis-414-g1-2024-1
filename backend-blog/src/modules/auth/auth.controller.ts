import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, Req } from '@nestjs/common';
import { AuthService } from './auth.service';
import { CreateAuthDto, LoginDTO } from './dto/create-auth.dto';
import { UpdateAuthDto } from './dto/update-auth.dto';
import { JwtAuthGuard } from 'src/guards/jwt-auth.guard';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Authenticate, UserAuthenticate } from 'src/decorators/user.decorator';
import { Permissions } from 'src/decorators/permissions.decorator';
import { PermissionsGuard } from 'src/guards/permission.guard';
@ApiTags('Autenticacion')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  login(@Body() logindto: LoginDTO) {
    return this.authService.loginWithCredentials(logindto);
  }

  @ApiBearerAuth()//para que aparesca el candadito de swagger y podamos colocar el token
  ///['permiso_1'] estos permisos estan en el metadata
  @Permissions('permiso_1')//PERMISOS QUE DEBE TENER EL USUARIO PARA PODER CONSUMIR ESTE SERVICIO
  @UseGuards(JwtAuthGuard,PermissionsGuard)//GUARDIANES PARA VALIDAR JwtAuthGuard=TOKEN ES VALIDO; PermissionsGuard=LOS PERMISOS DEL USUARIO
  @Post('verificar-token')//ruta http del servicio
  verifyToken(@UserAuthenticate() auth:Authenticate) {
    return this.authService.verifyToken(auth.token);
  }
}

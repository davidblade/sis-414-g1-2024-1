import { ApiProperty } from "@nestjs/swagger"
import { IsNotEmpty, IsString } from "class-validator"
//DATA TRANSFER OBJECT
//OBJETO DE TRANSFERENCIA DE DATOS
//MANDAR DATOS DEL CLIENTE AL SERVIDOR
//SERVIDOR AL CLIENTE
export class FileDTO{
    @ApiProperty()
    fileBase64:string

    @ApiProperty()
    name:string
    
    @ApiProperty()
    type:string
}

export class CreateBlogDTO {
    @IsNotEmpty({ message: 'Titulo No puede ser vacio' })
    @IsString({ message: 'Titulo debe ser un string' })
    @ApiProperty({required:true})
    title:string
    @ApiProperty()
    description:string
    
    @ApiProperty({type:FileDTO, isArray:true})
    files:FileDTO[]
}

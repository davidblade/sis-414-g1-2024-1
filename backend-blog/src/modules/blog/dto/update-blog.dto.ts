import { PartialType } from '@nestjs/mapped-types';
import { CreateBlogDTO } from './create-blog.dto';
import { IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UpdateBlogDto {
    @IsNotEmpty({ message: 'Titulo No puede ser vacio' })
    @IsString({ message: 'Titulo debe ser un string' })
    @ApiProperty({required:true})
    title:string
    @ApiProperty()
    description:string
}

import { Injectable } from '@nestjs/common';
import { CreateBlogDTO } from './dto/create-blog.dto';
import { UpdateBlogDto } from './dto/update-blog.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Post } from 'src/database/posts.schema';
import mongoose, { Model } from 'mongoose';
import { promises as fs } from 'fs';
import { join,basename,dirname,resolve } from 'path';
import { format } from "date-fns";

@Injectable()
export class BlogService {
  private readonly DOMAIN_SERVE = process.env.DOMAIN_SERVE
  private readonly filesPath = join(process.cwd(),'..', 'files-sis414')
  private readonly filesPublicPath = join(process.cwd(),'public')
  constructor(
    @InjectModel(Post.name) private postModel: Model<Post>
  ){}

  async create(createBlogDTO: CreateBlogDTO) {
    try {
      const savePost = new this.postModel({
        title:createBlogDTO.title,
        description:createBlogDTO.description,
        user:"aqui deberia ser un usuario"
      })
      
      
      //METODO 1
      //1.- RECUPERAR EL BASE64
      const filesPost = []
      for(const file of createBlogDTO.files){
        const nameID = new mongoose.Types.ObjectId()
        const prepareFile = {
          _id:nameID,
          nameOriginal:file.name,
          name:`${nameID.toString()}.${file.name.split('.').pop()}`,
          type:file.type
        }
        //2.- CONVERTIRLO A BUFFER (ARCHIVO BITE)
        const setRemoveHeaderBase64 = file.fileBase64.split(',')[1]
        const bufferFile = Buffer.from(setRemoveHeaderBase64, 'base64')
        //3.- GUARDARLO EN UNA CARPETA COMO ARCHIVO FISICO
        await fs.writeFile(`${this.filesPath}/${prepareFile.name}`, bufferFile) 
        filesPost.push(prepareFile)
      }
      
      // METODO 2 (NO SE HACE ESO... CHICHO MALO!!! ERROR!!! 403)
      //1.- RECUPERAR EL BASE64
      //2.- GUARDARLO EN LA BASE DE DATOS COMO BASE64 STRING
      
      savePost.set({
        files:filesPost
      })
      return await savePost.save()
      //return "Ok"
      //return `aqui vamos a crear un nuevo publicacion con los datos de ${JSON.stringify(createBlogDTO)}`; 
    } catch (error) {
      console.log(error)
      throw error 
    }
  }

  async findAll() {
    const getAllPosts = await this.postModel.find({active:true})
    for(const post of getAllPosts){
      for(const file of post.files){
        file.name = await this.readFile(`${file.name}`)
      }
    }
    return getAllPosts
    //return `This action returns all blog`;
  }

  async readFile(nameFileID:string){
    //recuperar el archivo fisico de la carpeta files-sis414
    const getFile = await fs.readFile(`${this.filesPath}/${nameFileID}`);
    //copiar el archivo fisico a la carpeta public
    await fs.writeFile(`${this.filesPublicPath}/${nameFileID}`, getFile) 

    return `${this.DOMAIN_SERVE}/${nameFileID}`

  }

  async findOne(id: string) {
    const getPostFindID = await this.postModel.findOne({_id:id,active:true})
    for(const file of getPostFindID.files){
      file.name = await this.readFile(`${file.name}.${file.type}`)
    }
    return getPostFindID
  }

  async update(id: string, updateBlogDto: UpdateBlogDto) {
    const updatePost = await this.postModel.findOne({_id:id})
    updatePost.set({
      title:updateBlogDto.title,
      description:updateBlogDto.description,
    })
    return await updatePost.save()

    //return `This action updates a #${id} blog`;
  }

  async remove(id: string) {
    ////EN UN SISTEMA REAL JAMAS SE ELIMINA NADA
    // const getPostDelete = await this.postModel.findOneAndDelete({_id:id})
    // return getPostDelete
    //LO QUE SI SE DEBE HACER ES :

    const deletePost = await this.postModel.findOne({_id:id})
    deletePost.set({
      active:false
    })
    return await deletePost.save()
    //return `This action removes a #${id} blog`;
  }
  async reportPDFPublications(){
    const getPublications = await this.postModel.find({active:true})
    const PuppeteerHTMLPDF = require("puppeteer-html-pdf");
    const namePdf = `${new mongoose.Types.ObjectId().toString()}.pdf`
    const htmlPDF = new PuppeteerHTMLPDF();
    const options = {
      format: "letter",
      path: `${this.filesPublicPath}/${namePdf}`, // you can pass path to save the file
    };
    htmlPDF.setOptions(options);
    
    const content = `
        <style> h1 {color:red;} </style>
        <h1>Welcome to puppeteer-html-pdf</h1>
        <table border="1">
            <thead>
              <tr>
                <th>TITULO DE PUBLICACION</th>
                <th>description</th>
                <th>Cantidad de archivos</th>
                <th>fecha de creacion</th>
              </tr>
            </thead>
            <tbody>
                  ${(()=>{
                    let tr = ''
                    for(const publication of getPublications){
                        let str = `
                        <tr>
                          <td>${publication.title}</td>
                          <td>${publication.description}</td>
                          <td>${publication.files.length}</td>
                          <td>${format(publication.createdAt, "dd/MM/yyyy HH:mm")}</td>
                        </tr>
                        `
                        tr += str
                    }
                    return tr
                  })()}
            </tbody>
        </table>
    `;
    
    try {
      const pdfBuffer = await htmlPDF.create(content)
      //const base64File = Buffer.from(pdfBuffer).toString("base64");
      return {
        nameFile:namePdf,
        urlFile:`${this.DOMAIN_SERVE}/${namePdf}`,
        //base64File
      }
    } catch (error) {
      console.log("PuppeteerHTMLPDF error", error);
    } 
  }
}

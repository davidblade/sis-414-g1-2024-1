import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards } from '@nestjs/common';
import { BlogService } from './blog.service';
import { CreateBlogDTO } from './dto/create-blog.dto';
import { UpdateBlogDto } from './dto/update-blog.dto';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/guards/jwt-auth.guard';

@ApiTags("BLOG")
@Controller('publication')
export class BlogController {
  constructor(private readonly blogService: BlogService) {}

  
  @ApiBearerAuth()//swagger
  @UseGuards(JwtAuthGuard)
  @Post('create-blog')
  //esta variable que es createBlogDTO va ser de tipo CreateBlogDTO
  // por lo tanto solo tiene los campos title y descrition los cuales tienen un tipo de dato
  // title es string y description string no puede recibir otros tipos de datos
  create(@Body() createBlogDTO: CreateBlogDTO) {
    return this.blogService.create(createBlogDTO);
  }

  @ApiOperation({summary:"retorna todos blogs",description:"Este end Point retorna todas las publiaciones activas"})
  @ApiResponse({ status: 200, description: 'Responde un json'})
  @ApiBearerAuth()//swagger
  @UseGuards(JwtAuthGuard)
  @Get('all-blogs')
  findAll() {
    return this.blogService.findAll();
  }

  @ApiBearerAuth()//swagger
  @UseGuards(JwtAuthGuard)
  @Get('blog-by-id/:id')
  findOne(@Param('id') id: string) {
    return this.blogService.findOne(id);
  }

  @ApiBearerAuth()//swagger
  @UseGuards(JwtAuthGuard)
  @Patch('update-by-id/:id')
  update(@Param('id') id: string, @Body() updateBlogDto: UpdateBlogDto) {
    return this.blogService.update(id, updateBlogDto);
  }

  @ApiBearerAuth()//swagger
  @UseGuards(JwtAuthGuard)
  @Delete('delete-by-id/:id')
  remove(@Param('id') id: string) {
    return this.blogService.remove(id);
  }

  @Get('create-pdf')
  async createReport(){
    return await this.blogService.reportPDFPublications()
  }
}

import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import {KEY_METADATA_PERMISSIONS} from '../decorators/permissions.decorator'
@Injectable()
export class PermissionsGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  matchRoles(permissions: string[], userPermissions: string[]) {
    return permissions.some((permission) => userPermissions.includes(permission))
  }

  canActivate(context: ExecutionContext): boolean {
    //esto saca los permisos del metadata
    const permissions = this.reflector.get<string[]>(KEY_METADATA_PERMISSIONS, context.getHandler());
    if (!permissions) {
      return true;
    }
    const request = context.switchToHttp().getRequest();
    const userPermissions = request.decodedData.permissions;
    return this.matchRoles(permissions, userPermissions);
  }
}